import fetch from "node-fetch";
import { writeFile } from "fs/promises";

async function fetchCharacters() {
  const response = await fetch("http://api.swgoh.gg/characters");
  return response.json();
}

function imageEntry(character) {
  return [character.base_id, character.image];
}

async function saveMap(map) {
  return writeFile("src/imageMap.json", JSON.stringify(map));
}

async function generateMap() {
  const characters = await fetchCharacters();
  const map = Object.fromEntries(characters.map(imageEntry));
  await saveMap(map);
}

generateMap()
  .then(() => console.log("map successfully generated"))
  .catch((error) => console.error("Could not generate map", error));
