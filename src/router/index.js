import { createRouter, createWebHistory } from "vue-router";
import PlayerFetcher from "/@/components/PlayerFetcher.vue";

const routes = [
  {
    path: "/explore/:allyCodeToFetch",
    name: "ExploreByAllyCode",
    component: PlayerFetcher,
  },
  {
    path: "/",
    name: "Explore",
    component: PlayerFetcher,
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
