import primary_stat_names from "/@/components/primary_stat_names.json";
import stat_names from "/@/components/stat_names.json";
import minMaxStats from "./min_max_stats.json";
import imageUrls from "./imageMap.json";

const primary6dotValues = {
  55: 16,
  56: 24,
  49: 20,
  48: 8.5,
  17: 30,
  18: 35,
  5: 32,
  16: 42,
  53: 20,
  46: 30,
  47: 35,
};

export function calcPercentage(secondary, pips) {
  if (secondary.unitStat == 0) {
    return secondary.value / 100;
  }

  if (!minMaxStats[secondary.unitStat]) {
    return -1;
  }

  let { min, max, "6e": increase } = minMaxStats[secondary.unitStat];

  let avg = secondary.value / secondary.roll;
  if (pips == 6) {
    avg = avg / increase;
  }
  avg = avg - min;
  return avg / (max - min);
}

export function statsToObject(stats) {
  return {
    value: Number.isInteger(stats[2]) ? stats[2] : stats[2].toFixed(2),
    unitSign: getUnit(stats[1]),
    name: stats[0],
    roll: stats[3],
    unitStat: stats[1],
  };
}

export function getUnit(unitStat) {
  switch (unitStat) {
    case 1: // Health
    case 5: // Speed
    case 28: // Protection
    case 41: // Offense
    case 42: // Defense
      return "";
    default:
      return "%";
  }
}

export function getMinMax(secondary, pips) {
  if (secondary.unitStat == 0) {
    return [0, 100];
  }
  if (!minMaxStats[secondary.unitStat]) {
    return [0, 0];
  }

  let { min, max, "6e": increase } = minMaxStats[secondary.unitStat];
  min = min * secondary.roll;
  max = max * secondary.roll;
  if (pips == 6) {
    min = min * increase;
    max = max * increase;
  }
  const decimals = secondary.unitSign == "%" ? 2 : 0;
  return [min.toFixed(decimals), max.toFixed(decimals)];
}

function getPrimary6Value(unitStat) {
  return primary6dotValues[unitStat];
}

function get6Value(unitStat, value) {
  if (unitStat == 5) {
    return value + 1;
  }
  let { "6e": increase } = minMaxStats[unitStat];
  let unit = getUnit(unitStat);
  let value6e = value * increase;
  return +value6e.toFixed(unit == "%" ? 2 : 0);
}

export function simulate6dots(mod) {
  if (mod.pips == 6) {
    return mod;
  }
  const secondaries = mod.secondaryStat.map((stat) => ({
    ...stat,
    value: get6Value(stat.unitStat, stat.value),
  }));
  let primary = mod.primaryStat;
  primary.value = getPrimary6Value(primary.unitStat);
  return {
    ...mod,
    primaryStat: primary,
    secondaryStat: secondaries,
    pips: 6,
    tier: 1,
  };
}

export function transformPlayer(player) {
  if (!player || !player.roster) {
    return [];
  }
  return player.roster.flatMap(({ defId, mods }) =>
    mods.map((mod) => ({
      ...mod,
      set_name: primary_stat_names[mod.set],
      toon: "toon" in mod ? mod.toon : defId,
      slot: mod.slot - 1,
      primaryStat: {
        ...mod.primaryStat,
        name: stat_names[mod.primaryStat.unitStat],
      },
      secondaryStat: mod.secondaryStat.map((stat) => ({
        ...stat,
        unitSign: getUnit(stat.unitStat),
        value: Number.isInteger(stat.value)
          ? +stat.value
          : +stat.value.toFixed(2),
        name: stat_names[stat.unitStat],
      })),
    }))
  );
}

export function imageUrlForCharacter(character) {
  if (!(character in imageUrls)) {
    return "";
  }
  return imageUrls[character];
}
